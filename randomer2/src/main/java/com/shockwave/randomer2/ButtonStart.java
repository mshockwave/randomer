package com.shockwave.randomer2;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.Button;
import android.widget.RelativeLayout;

public class ButtonStart extends Button{
	
	public final float BUT_START_LAYOUT_PERCENT_W = 0.33f;

	boolean start_but_ondraw_flag = false;
	
	public ButtonStart(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	public ButtonStart(Context context) {
		super(context);
	}
	
	@Override
	protected void onDraw(Canvas canvas){
		super.onDraw(canvas);
		
		//Avoiding re-calculating while redrawing
		if( ! start_but_ondraw_flag ){
			set_button();
			start_but_ondraw_flag = true;
		}
		
	}
	
	/*
	 * Handling the job of resizing and putting start button in correct position*/
	private final void set_button(){
		float but_w = Common.AppProp.width_px * BUT_START_LAYOUT_PERCENT_W;
        float but_h = this.getHeight() * ( but_w / this.getWidth() );
        RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams( (int)but_w, (int)but_h );
        lp.addRule(RelativeLayout.BELOW, R.id.FrameLayout_show);
        lp.addRule(RelativeLayout.CENTER_HORIZONTAL);
        this.setLayoutParams(lp);
	}
	
}
