package com.shockwave.randomer2;

import android.util.Log;

public class Common{
	
	public static class RingProp{
		public static float center_x, center_y;
		public static float radius_in_px, radius_out_px;
		public static float strokeoffset_px;
		
		public static int view_h,view_w;
	}
	
	public static class PointerProp{
		public static final String lap_count_default = "3";
		
		public static final int speed_high_drt = 1;
		public static final int speed_medium_drt = 2;
		public static final int speed_low_drt = 3;

        public static final int spin_t_accelerate = -(1<<1);
        public static final int spin_t_overshoot = -(1<<2);
        public static final int spin_t_roulette = -(1<<3);
        public static final int spin_t_linear = -(1<<4);
	}
	
	public static class SeqNumProp{
		public static final int EMPTY = 100000;
		public static final int VALUE_MAX = 1500;
		public static final int VALUE_MIN = -1500;
	}
	
	public static class FrameProp{
		public static float padding_all;
	}
	
	public static class AppProp{
		public static float screen_d = 0.0f;
		public static int width_px, height_px;
	}
	
	public static class DBProp{
		public static final int ERROR_DB_INSERT_FAIL = -0x043;
		public static final int ERROR_DB_EDIT_FAIL = -0x044;
		public static final int ERROR_DB_REMOVE_FAIL = -0x045;
		public static final int ERROR_DB_NO_SUCH_TABLE = -0x046;
		
	}
	
	public static class PrefKeys{
		public static final String POINTER_LAP_COUNT = "pref_pointer_lap_count";
		public static final String POINTER_SPEED = "pref_pointer_speed";
        public static final String POINTER_SPIN_T = "pref_pointer_spin_type";
	}
	
	public static DBHelper db = null;//shared database instance
	
	
	/*
	 * Transfer dip length to pixel length
	 * @param dip: input dip*/
	public static final float CastToPx(float dip){
		return ( AppProp.screen_d * dip );
	}
	
	/*
	 * Debug log wrapper
	 * @param msg: debug message*/
	public static final void DEBUG(String msg){
		String tag = "randomer2_debug";
		
		Log.d(tag, msg);
	}
}
