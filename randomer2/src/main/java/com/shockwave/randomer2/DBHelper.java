package com.shockwave.randomer2;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;

public class DBHelper extends SQLiteOpenHelper {
	
	/*
	 * Working principle:
	 * index: table that holds the _id and the user table name
	 * data:  table that holds several user table data
	 * Actually the user table was just a bundle of rows in the data table.
	 * If we want to query a column of a user table, we needs to convert the user table name into _id,
	 * which is the user table index number(index_) at the same time,and then use the _id
	 * (telling us which rows in data table represent the very user table) 
	 * to query in the data table.
	 */
	
	final static int DB_VERSION = 1;
	final static String DB_NAME = "database.db";
	
	final String CREATE_TABLE_DATA = "CREATE TABLE "+
									 "data( "+
									 "_id INTEGER PRIMARY KEY AUTOINCREMENT, "+
									 "index_ INTEGER UNSIGNED, "+
									 "item NVARCHAR(20), "+
									 "weight INTEGER DEFAULT 1, "+
									 "FOREIGN KEY(index_) "+
									 "REFERENCES index_data(_id) "+
									 ");";
	
	final String CREATE_TABLE_INDEX = "CREATE TABLE "+
			 						  "index_data( "+
			 						  "_id INTEGER PRIMARY KEY AUTOINCREMENT, "+
			 						  "table_name NVARCHAR(20) "+
			 						  ");";

	
	public DBHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
		
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		
		try{
			db.execSQL(CREATE_TABLE_INDEX);
		}catch(Exception e){
			Common.DEBUG("Create db index fail: "+e.getMessage());
		}
		
		try{
			db.execSQL(CREATE_TABLE_DATA);
		}catch(Exception e){
			Common.DEBUG("Create db data fail: "+e.getMessage());
		}
	}
	
	public void create_table(String name) throws Exception{
		ContentValues data = new ContentValues();
		data.put("table_name", toUTF8(name));
		
		try{
			this.getWritableDatabase().insertOrThrow("index_data", null, data);
		}catch(Exception e){
			throw new Exception(""+Common.DBProp.ERROR_DB_INSERT_FAIL);
		}
		
	}
	
	private String toUTF8(String original){
		try {
			return  new String( original.getBytes("UTF-8"), "UTF-8" );
		} catch (UnsupportedEncodingException e) {
			Common.DEBUG("No UTF-8 encoding");
			return original;
		}
	}
	
	//Get the table index
	public int getIndex(String table_name) throws Exception{
		Cursor cursor;
		
		if( ( cursor = this.getReadableDatabase().query("index_data", new String[]{"_id"}, "table_name = ?", new String[]{toUTF8(table_name)}, null, null, null) ) == null)
			throw new Exception(""+Common.DBProp.ERROR_DB_NO_SUCH_TABLE);
		cursor.moveToFirst();
		int tmp = cursor.getInt(0);
		cursor.close();
		return tmp;
	}
	
	//Get the total table number
	public int getTableCount(){
		Cursor cursor;
		
		cursor = this.getReadableDatabase().rawQuery("SELECT _id FROM index_data", null);
		int tmp = cursor.getCount();
		cursor.close();
		return tmp;
	}
	
	//Dump all the tables' name
	public ContentValues getTableName(){
		ContentValues result = new ContentValues();
		Cursor cursor;
		Byte b = 1;
		
		cursor = this.getReadableDatabase().query("index_data", new String[]{"table_name"}, null, null, null, null, null);
		if(cursor == null)
			return null;//No table
		
		cursor.moveToFirst();
		for(int i = 0 ; i < cursor.getCount() ; i++){
			result.put( toUTF8(cursor.getString(0)), b );
			
			cursor.moveToNext();
		}
		
		return result;
	}
	
	public String[] getTableNameArray(){
		Cursor cursor;
		
		cursor = this.getReadableDatabase().query("index_data", new String[]{"table_name"}, null, null, null, null, null);
		if(cursor == null)
			return null;//No table
		
		cursor.moveToFirst();
		String[] list = new String[cursor.getCount()];
		for(int i = 0 ; i < cursor.getCount() ; i++){
			list[i] = toUTF8(cursor.getString(0));
			
			cursor.moveToNext();
		}
		
		return list;
	}
	
	//Get item number of a table
	public int getRowCount(int table_index){
		Cursor cursor;
		
		cursor = this.getReadableDatabase().query("data", new String[]{"_id"}, "index_ = ?", new String[]{""+table_index}, null, null, null);
		int tmp = cursor.getCount();
		cursor.close();
		return tmp;
	}
	
	//New row
	public void insert_new(int table_index, String item, int weight) throws Exception{
		ContentValues data;
		
		data = new ContentValues();
		data.put("index_", table_index);
		data.put("item", toUTF8(item));
		data.put("weight", weight);
		try{
			this.getWritableDatabase().insertOrThrow("data", null, data);
		}catch(Exception e){
			throw new Exception(""+Common.DBProp.ERROR_DB_INSERT_FAIL);
		}
		
	}
	
	//Query - dump the whole table items
	public Bundle query(int table_index) throws Exception{
		int count;
		Bundle result = new Bundle();
		ArrayList<String> name = new ArrayList<String>();
		ArrayList<Integer> weight = new ArrayList<Integer>();
		Cursor cursor;
		
		count = getRowCount(table_index);
		
		cursor = this.getReadableDatabase().query("data", new String[]{"item","weight"}, "index_ = "+table_index, null, null, null, null);
		cursor.moveToFirst();
		for(int i = 0 ; i < count ; i++){
			name.add( toUTF8(cursor.getString(0)) );
			weight.add(cursor.getInt(1));
			
			cursor.moveToNext();
		}
		cursor.close();
		result.putStringArrayList("name", name);
		result.putIntegerArrayList("weight", weight);
		
		return result;
		
	}
	
	
	
	//Edit weight
	public void edit(int table_index, String item_name, int new_weight) throws Exception{
		ContentValues data;
		
		data = new ContentValues();
		data.put("weight", new_weight);
		if( this.getWritableDatabase().update("data", data, "index_ = "+table_index+" AND "+"item = ?", new String[]{toUTF8(item_name)}) == 0 )//No row was updated
			throw new Exception(""+Common.DBProp.ERROR_DB_EDIT_FAIL);
	}
	
	//Edit item
	public void edit(int table_index, String item_name, String new_name) throws Exception{
		ContentValues data;
		
		data = new ContentValues();
		data.put("item", toUTF8(new_name));
		if( this.getWritableDatabase().update("data", data, "index_ = "+table_index+" AND "+"item = ?", new String[]{toUTF8(item_name)}) == 0 )//No row was updated
			throw new Exception(""+Common.DBProp.ERROR_DB_EDIT_FAIL);
	}
	
	//Rename Table
	public void rename_table(int table_index, String new_name) throws Exception{
		ContentValues data;
		
		data = new ContentValues();
		data.put("table_name", toUTF8(new_name));
		if( this.getWritableDatabase().update("index_data", data, "_id = ?", new String[]{""+table_index}) == 0 )//No rows was updated
			throw new Exception(""+Common.DBProp.ERROR_DB_EDIT_FAIL);
	}
	
	//Remove row
	public void remove(int table_index, String item_name) throws Exception{
		
		if( this.getWritableDatabase().delete("data", "item = ?"+" AND "+"index_ = "+table_index, new String[]{toUTF8(item_name)}) == 0)
			throw new Exception(""+Common.DBProp.ERROR_DB_REMOVE_FAIL);
		
	}
	
	//Remove Table
	public void remove_table(int table_index) throws Exception{
		
		//remove data in the data table
		this.getWritableDatabase().delete("data", "index_ = "+table_index, null);
		//remove the index in the index table
		if( this.getWritableDatabase().delete("index_data", "_id = "+table_index, null) == 0)
			throw new Exception(""+Common.DBProp.ERROR_DB_REMOVE_FAIL);
		
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		
		
	}

}
