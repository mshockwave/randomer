	package com.shockwave.randomer2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class FragmentItem extends Fragment implements Main.BackButtonListener{

	ListView item_list;
	
	ArrayAdapter<String> adapter = null;
	int table_index = 0;
	List<String> items = null, items_name = null;
	List<Integer> items_weight = null;
	Bundle data = null;
	List<Integer> values = null;//sequence number values cache
	
	RelativeLayout add_item;
	TextView text_add;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		Common.DEBUG("Fragment Item onCreate");
		
		data = this.getArguments();
		if(items != null) items.clear();
		if(items_name != null) items_name.clear();
		if(items_weight != null) items_weight.clear();
		if(adapter != null) adapter.clear();
	}
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		
		Common.DEBUG("Fragment Item onAttach");
	}
	
	@Override
	public void onPause(){
		super.onPause();
		Common.DEBUG("Fragment Item onPause");
	}
	
	@Override
	public void onStart(){
		super.onStart();
		Common.DEBUG("Fragment Item onStart");
	}
	
	@Override
	public void onResume(){
		super.onResume();
		Common.DEBUG("Fragment Item onResume");
	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		Common.DEBUG("Fagment Item onDestroy");
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		Common.DEBUG("Fragment Item onCreateView");
		((Main)this.getActivity()).setBackButtonListener(this);
		//Default layout
		View this_view = inflater.inflate(R.layout.fragment_item_default, null);
		
		if(this.data != null){
			String table_name = data.getString("table_name");
			getActivity().setTitle(table_name);
			items = new ArrayList<String>();
			Bundle result;
			this_view = inflater.inflate(R.layout.fragment_item, null);
			
			//UI part
			item_list = (ListView)this_view.findViewById(R.id.fragment_item_listview);
			add_item = (RelativeLayout)this_view.findViewById(R.id.add_new_item);
			text_add = (TextView)this_view.findViewById(R.id.fragment_item_text_add);
			
			adapter = ( new ArrayAdapter<String>( getActivity().getBaseContext(), R.layout.list_item, items) );
			item_list.setAdapter(adapter);
			ItemClick itemclk = new ItemClick();
			item_list.setOnItemClickListener(itemclk);
			add_item.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View arg0) {
					DialogEdit dialog_add = new DialogEdit();
					dialog_add.show();
				}
				
			});
			
			//Data Fetching Part
			try{
				table_index = Common.db.getIndex(table_name);
				result = Common.db.query( table_index );
			}catch(Exception e){
				result = null;
				table_index = 0;
				Common.DEBUG("No such table: "+table_name);
			}
			
			if(result != null){
				items_name = result.getStringArrayList("name");
				items_weight = result.getIntegerArrayList("weight");
				Iterator<String> it_name_list = items_name.iterator();
				Iterator<Integer> it_weight_list = items_weight.iterator();
				String first_name = it_name_list.next();
				int first_weight = it_weight_list.next();
				
				if(first_weight < 0){
					addItemToggle(false);
					
					values = new ArrayList<Integer>();
					int step = 1;
					
					if(first_weight == -1){//handling first value pair
						values.add( Integer.valueOf(first_name) );
					}else{
						try{
							step = Integer.valueOf( first_name.substring(1, first_name.length()-1) );
						}catch(Exception e){
							step = 1;
						}
					}
					
					while(it_name_list.hasNext() && it_weight_list.hasNext()){
						int tmp_weight = it_weight_list.next();
						String tmp_name = it_name_list.next();
						
						if(tmp_weight == -1){
							values.add( Integer.valueOf(tmp_name) );
						}else{
							try{
								step = Integer.valueOf( tmp_name.substring(1, tmp_name.length()-1) );
							}catch(Exception e){
								step = 1;
							}
						}
					}
					
					
					if(values.get(0) > values.get(1)){
						int tmp = values.get(0);
						values.set(0, values.get(1));
						values.set(1, tmp);
					}
					values.add(step);
					
					items.add( values.get(0)+" ~ "+values.get(1)+" "+getResources().getString(R.string._new_number_step)+" "+values.get(2) );
				}else{
					addItemToggle(true);
					
					items.add(first_name+" ("+first_weight+")");
					
					while(it_name_list.hasNext() && it_weight_list.hasNext()){
						int tmp_weight = it_weight_list.next();
						String tmp_name = it_name_list.next();
						
						items.add( tmp_name+" ("+tmp_weight+")" );
						
					}
				}
				adapter.notifyDataSetChanged();
			}
		}
		
		return this_view;
	}
	
	private void addItemToggle(boolean swtch){
		if(swtch){
			text_add.setTextColor(getResources().getColor(android.R.color.black));
			add_item.setClickable(true);
		}else{
			text_add.setTextColor(getResources().getColor(R.color.gray));
			add_item.setClickable(false);
		}
	}
	
	protected class ItemClick implements AdapterView.OnItemClickListener{
		DialogEdit dialog;
		
		@Override
		public void onItemClick(AdapterView<?> arg0, View view, int position, long id) {
			if(values != null){
				dialog = new DialogEdit(values.get(0), values.get(1), values.get(2));
				dialog.show();
			}else{
				dialog = new DialogEdit(items_name.get((int)id), items_weight.get((int)id), (int)id);
				dialog.show();
			}
		}
		
	}

	protected class DialogEdit implements DialogInterface.OnClickListener{
		AlertDialog.Builder dialog;
		View dialog_view;
		EditText edit_name = null, edit_weight = null, edit_start = null, edit_end = null, edit_step = null;
		String f_start, f_end, f_step, f_name;
		int position;
		boolean edit_mode = true;
		
		public DialogEdit(){//add mode
			dialog = new AlertDialog.Builder(getActivity());
			dialog_view = getActivity().getLayoutInflater().inflate(R.layout.new_user_dialog, null);
			
			edit_name = (EditText)dialog_view.findViewById(R.id.new_user_dialog_name);
			edit_weight = (EditText)dialog_view.findViewById(R.id.new_user_dialog_weight);
			
			edit_mode = false;
			
			init();
		}
		
		public DialogEdit(String formal_name, int formal_weight, int position){//edit user define 
			dialog = new AlertDialog.Builder(getActivity());
			dialog_view = getActivity().getLayoutInflater().inflate(R.layout.new_user_dialog, null);
			
			edit_name = (EditText)dialog_view.findViewById(R.id.new_user_dialog_name);
			edit_name.setText(formal_name);
			f_name = formal_name;
			edit_weight = (EditText)dialog_view.findViewById(R.id.new_user_dialog_weight);
			edit_weight.setText(""+formal_weight);
			
			this.position = position;
			
			edit_mode = true;
			
			init();
		}
		
		public DialogEdit(int start, int end, int step){//edit sequence numbers
			dialog = new AlertDialog.Builder(getActivity());
			dialog_view = getActivity().getLayoutInflater().inflate(R.layout.fragment_new_number, null);
			
			edit_start = (EditText)dialog_view.findViewById(R.id.fragment_new_num_bottom);
			edit_start.setText(""+start);
			f_start = ""+start;
			edit_end = (EditText)dialog_view.findViewById(R.id.fragment_new_num_top);
			edit_end.setText(""+end);
			f_end = ""+end;
			edit_step = (EditText)dialog_view.findViewById(R.id.fragment_new_num_step);
			edit_step.setText(""+step);
			f_step = "_"+step+"_";
			
			edit_mode = true;
			
			init();
		}
		
		private void init(){
			dialog.setView(dialog_view);
			dialog.setPositiveButton(getResources().getString(R.string.dialog_ok), this);
			dialog.setNegativeButton(getResources().getString(R.string.dialog_cancel), this);
			
			if(edit_mode)
				dialog.setNeutralButton(getResources().getString(R.string.dialog_delete), this);
		}
		
		public void show(){
			dialog.create().show();
		}
		
		private void edit_num_handler(){
			int[] num = new int[3];
			
			try{
				num[0] = Integer.valueOf(edit_start.getText()+"");
			}catch(Exception e){//empty
				num[0] = Common.SeqNumProp.EMPTY;
			}
			
			try{
				num[1] = Integer.valueOf(edit_end.getText()+"");
			}catch(Exception e){//empty
				num[1] = Common.SeqNumProp.EMPTY;
			}
			
			try{
				num[2] = Integer.valueOf(edit_step.getText()+"");
			}catch(Exception e){//empty
				num[2] = Common.SeqNumProp.EMPTY;
			}
			
			if( num[1] == Common.SeqNumProp.EMPTY || num[0] == Common.SeqNumProp.EMPTY || num[2] == Common.SeqNumProp.EMPTY){//empty
				Toast.makeText(getActivity(), R.string.seq_num_format_error, Toast.LENGTH_LONG).show();
				return;
			}else if( !num_check(num[0], num[1], num[2]) ){
				Toast.makeText(getActivity(), R.string.seq_num_format_error, Toast.LENGTH_LONG).show();
				return;
			}else{
				try{
					Common.db.edit(table_index, f_start, ""+num[0]);
					Common.db.edit(table_index, f_end, ""+num[1]);
					Common.db.edit(table_index, f_step, "_"+Math.abs(num[2])+"_");
				}catch(Exception e){
					Common.DEBUG("edit seq num fail");
				}
			}
			
			if(num[0] > num[1]){
				values.set(0, num[1]);
				values.set(1, num[0]);
			}else{
				values.set(0, num[0]);
				values.set(1, num[1]);
			}
			values.set(2, num[2]);
			
			items.set(0, values.get(0)+" ~ "+values.get(1)+" "+getResources().getString(R.string._new_number_step)+" "+values.get(2) );
		}
		
		private final boolean num_check(int start, int end, int step){
			if( start > Common.SeqNumProp.VALUE_MAX || start < Common.SeqNumProp.VALUE_MIN || 
				end > Common.SeqNumProp.VALUE_MAX || end < Common.SeqNumProp.VALUE_MIN ) return false;
			if( start == end || step == 0) return false;
			if( ((end - start < 0) ^ (step < 0)) ) return false;
			if(Math.abs(end - start) < Math.abs(step)) return false;
			
			return true;
		}
		
		private void edit_user_handler(){
			String name = (edit_name.getText()+"").trim();
			if(name.equals("")) return;
			if( (!f_name.equals(name)) && items_name.contains(name) ){
				Toast.makeText(getActivity(), R.string._new_user_error_item_exist, Toast.LENGTH_LONG).show();
				return;
			}
			
			int weight;
			try{
				weight = Integer.valueOf( edit_weight.getText()+"" );
			}catch(Exception e){
				weight = 1;
			}
			
			try{
				Common.db.edit(table_index, f_name, weight);
				Common.db.edit(table_index, f_name, name);
			}catch(Exception e){
				Common.DEBUG("edit "+f_name+" fail");
			}
			
			items_name.set(position, name);
			items_weight.set(position, weight);
			items.set(position, name+" ("+weight+")");
		}
		
		private void add_user_handler(){
			String name = edit_name.getText()+"";
			if(name.equals("")) return;
			if( items_name.contains(name) ){
				Toast.makeText(getActivity(), R.string._new_user_error_item_exist, Toast.LENGTH_LONG).show();
				return;
			}
			
			int weight;
			try{
				weight = Integer.valueOf( edit_weight.getText()+"" );
			}catch(Exception e){
				weight = 1;
			}
			
			try{
				Common.db.insert_new(table_index, name, weight);
			}catch(Exception e){
				Common.DEBUG("add "+name+" fail");
			}
			
			items_name.add(name);
			items_weight.add(weight);
			items.add(name+" ("+weight+")");
		}
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			switch(which){
			case DialogInterface.BUTTON_POSITIVE:
				if(edit_start != null){
					edit_num_handler();
				}else{
					if(edit_mode){
						edit_user_handler();
					}else{
						add_user_handler();
					}
				}
				adapter.notifyDataSetChanged();
				break;
			case DialogInterface.BUTTON_NEUTRAL:
				if(edit_start != null){
					try{
						Common.db.remove_table(table_index);
						adapter.clear();
					}catch(Exception e){
						Common.DEBUG("Remove table index "+table_index+" fail");
					}
				}else{
					try{
						Common.db.remove(table_index, f_name);
						items.remove(position);
						items_name.remove(position);
						items_weight.remove(position);
					}catch(Exception e){
						Common.DEBUG("Remove item "+f_name+" fail");
					}
					
					if(items.size() == 0){//empty
						try{
							Common.db.remove_table(table_index);
							adapter.clear();
							addItemToggle(false);
							break;
						}catch(Exception e){
							Common.DEBUG("Remove table index "+table_index+" fail");
						}
					}
					
					adapter.notifyDataSetChanged();
					
				}
				break;
			case DialogInterface.BUTTON_NEGATIVE:
				dialog.cancel();
				break;
			}
		}
		
	}

	@Override
	public void BackButtonHandler() {
		((Main)getActivity()).selectTab(0, null);
	}
}
