package com.shockwave.randomer2;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

public class FragmentNewNumber extends Fragment{
	
	
	EditText edit_bottom, edit_top, edit_step;
	
	LockUnlockListener lockCallback;
	
	public interface LockUnlockListener{
		public void onLockUnlock(boolean lock, int bit_mask);
	}
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		
		try{
			lockCallback = (LockUnlockListener)activity;
		}catch(Exception e){
			Common.DEBUG(activity.toString()+"hasn't implments interface");
		}
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		View this_view = inflater.inflate(R.layout.fragment_new_number, null);
		
		edit_bottom = (EditText)this_view.findViewById(R.id.fragment_new_num_bottom);
		edit_top = (EditText)this_view.findViewById(R.id.fragment_new_num_top);
		edit_step = (EditText)this_view.findViewById(R.id.fragment_new_num_step);
		
		NumChecker nck = new NumChecker();
		edit_bottom.addTextChangedListener(nck);
		edit_top.addTextChangedListener(nck);
		edit_step.addTextChangedListener(nck);
		
		return this_view;
	}

	protected class NumChecker implements TextWatcher{

		int[] values = new int[3];
				
		@Override
		public void afterTextChanged(Editable s) {
			try{
				values[0] = Integer.valueOf(edit_bottom.getText()+"");
			}catch(Exception e){//empty
				values[0] = Common.SeqNumProp.EMPTY;
			}
			
			try{
				values[1] = Integer.valueOf(edit_top.getText()+"");
			}catch(Exception e){//empty
				values[1] = Common.SeqNumProp.EMPTY;
			}
			
			try{
				values[2] = Integer.valueOf(edit_step.getText()+"");
			}catch(Exception e){//empty
				values[2] = Common.SeqNumProp.EMPTY;
			}
			
			Common.DEBUG("start: "+values[0]+" end: "+values[1]+" step: "+values[2]);
			
			if( values[1] == Common.SeqNumProp.EMPTY || values[0] == Common.SeqNumProp.EMPTY || values[2] == Common.SeqNumProp.EMPTY ){//empty
				lockCallback.onLockUnlock(true, NewTableActivity.MASK_COMMIT_BUT);
				edit_bottom.setTextColor(getResources().getColor(R.color.black));
				edit_top.setTextColor(getResources().getColor(R.color.black));
				edit_step.setTextColor(getResources().getColor(R.color.black));
			}else if( !check(values[0], values[1], values[2]) ){
				lockCallback.onLockUnlock(true, NewTableActivity.MASK_COMMIT_BUT);
				edit_bottom.setTextColor(getResources().getColor(R.color.red));
				edit_top.setTextColor(getResources().getColor(R.color.red));
				edit_step.setTextColor(getResources().getColor(R.color.red));
			}else{
				NewTableActivity.items.clear();
				NewTableActivity.items.put(values[0]+"", -1);
				NewTableActivity.items.put(values[1]+"", -1);
				NewTableActivity.items.put("_"+Math.abs(values[2])+"_", -2);//step's negative or positive doesn't matter
				
				lockCallback.onLockUnlock(false, NewTableActivity.MASK_COMMIT_BUT);
				edit_bottom.setTextColor(getResources().getColor(R.color.black));
				edit_top.setTextColor(getResources().getColor(R.color.black));
				edit_step.setTextColor(getResources().getColor(R.color.black));
			}
		}
		
		protected final boolean check(int start, int end, int step){
			if( start > Common.SeqNumProp.VALUE_MAX || start < Common.SeqNumProp.VALUE_MIN || 
				end > Common.SeqNumProp.VALUE_MAX || end < Common.SeqNumProp.VALUE_MIN ) return false;
			if( start == end || step == 0) return false;
			if( ((end - start < 0) ^ (step < 0)) ) return false;
			if(Math.abs(end - start) < Math.abs(step)) return false;
			
			return true;
		}

		@Override
		public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
			
		}

		@Override
		public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
			
		}
		
	}
	
	
}
