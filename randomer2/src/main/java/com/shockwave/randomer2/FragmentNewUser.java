package com.shockwave.randomer2;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.app.AlertDialog;
//import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Toast;
//import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

public class FragmentNewUser extends Fragment{
	
	SimpleAdapter adapter;
	ArrayList<HashMap<String,Object>> item_list = new ArrayList<HashMap<String,Object>>();
	HashMap<String, Object> empty = new HashMap<String, Object>();
	ListView list;
	LockUnlockListener lockCallback;
	
	public interface LockUnlockListener{
		public void onLockUnlock(boolean lock, int bit_mask);
	}
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		
		try{
			lockCallback = (LockUnlockListener)activity;
		}catch(Exception e){
			Common.DEBUG(activity.toString()+" haven't implements interface");
		}
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		View this_view = inflater.inflate(R.layout.fragment_new_user, null);
		list = (ListView)this_view.findViewById(R.id.fragment_new_user_list);
		
		HashMap<String, Object> first_item = new HashMap<String, Object>();
		first_item.put("icon", R.drawable.ic_menu_add);
		first_item.put("text", this.getResources().getString(R.string._new_user_item_first));
		item_list.add(first_item);
		
		item_list.add(empty);
		
		adapter = new SimpleAdapter(getActivity().getBaseContext(), 
										  item_list, 
										  R.layout.list_item_user, 
										  new String[]{"icon","text","weight"}, 
										  new int[]{R.id.list_item_user_icon, R.id.list_item_user_text, R.id.list_item_user_weight});
		list.setAdapter(adapter);
		
		ItemClick onitemclick = new ItemClick();
		list.setOnItemClickListener(onitemclick);
		
		return this_view;
	}
	
	protected class ItemClick implements AdapterView.OnItemClickListener{

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
			AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
			int length = item_list.size();
			
			if(position == 0){
				LayoutInflater inflater = getActivity().getLayoutInflater();
				View dialog_view = inflater.inflate(R.layout.new_user_dialog, null);
				
				dialog.setView(dialog_view);
				AddEdit additem = new AddEdit(dialog_view);
				dialog.setPositiveButton(getResources().getString(R.string.dialog_ok), additem);
				dialog.setNegativeButton(getResources().getString(R.string.dialog_cancel), additem);
				dialog.create().show();
			}else if( position != (length - 1) ){
				LayoutInflater inflater = getActivity().getLayoutInflater();
				View dialog_view = inflater.inflate(R.layout.new_user_dialog, null);
				
				String name_prev = (String) item_list.get(position).get("text");
				int weight_prev = NewTableActivity.items.getAsInteger(name_prev);
				EditText dialog_name_prev = (EditText)dialog_view.findViewById(R.id.new_user_dialog_name);
				EditText dialog_weight_prev = (EditText)dialog_view.findViewById(R.id.new_user_dialog_weight);
				dialog_name_prev.setText(name_prev);
				dialog_weight_prev.setText( weight_prev+"" );
				
				dialog.setView(dialog_view);
				AddEdit edititem = new AddEdit(dialog_view, position, name_prev);
				dialog.setPositiveButton(getResources().getString(R.string.dialog_ok), edititem);
				dialog.setNegativeButton(getResources().getString(R.string.dialog_cancel), edititem);
				dialog.setNeutralButton(getResources().getString(R.string.dialog_delete), edititem);
				dialog.create().show();
			}
		}
		
	}

	protected class AddEdit implements DialogInterface.OnClickListener{
		View dialog_view;
		int position = 0;
		String name_prev = null;
		
		//Add mode
		public AddEdit(View _dialog_view){
			this.dialog_view = _dialog_view;
			
		}
		
		//Edit mode
		public AddEdit(View _dialog_view, int _position, String _name_prev){//position in the arraylist equals the position in the listview
			this.dialog_view = _dialog_view;
			this.position = _position;
			this.name_prev = _name_prev;
		}
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			switch(which){
			case DialogInterface.BUTTON_POSITIVE:
				EditText input = (EditText)dialog_view.findViewById(R.id.new_user_dialog_name);
				String tmp_text,tmp_weight;
				
				if( (tmp_text = (input.getText()+"").trim()).equals("") ){
					dialog.cancel();
					break;
				}
				
				if( NewTableActivity.items.containsKey(tmp_text) ){
					if( name_prev == null || (!tmp_text.equals(name_prev))){	
						dialog.cancel();
						Toast.makeText(getActivity(), R.string._new_user_error_item_exist, Toast.LENGTH_LONG).show();
						break;
					}
				}
				
				HashMap<String, Object> _item = new HashMap<String, Object>(); 
				
				_item.put("icon", R.drawable.btn_radio);
				_item.put("text", tmp_text);
				
				input = (EditText)dialog_view.findViewById(R.id.new_user_dialog_weight);
				if( (tmp_weight = input.getText()+"").equals("") )
					tmp_weight = "1";
				_item.put("weight", "("+tmp_weight+")");
				
				
				if(position == 0){
					item_list.add(1, _item);//add mode
				}else{
					NewTableActivity.items.remove( (String)item_list.get(position).get("text") );
					item_list.set(position, _item);//edit mode
				}
				NewTableActivity.items.put(tmp_text, Integer.valueOf(tmp_weight));
				
				adapter.notifyDataSetChanged();
				lockCallback.onLockUnlock(false, NewTableActivity.MASK_COMMIT_BUT);
				break;
			
			case DialogInterface.BUTTON_NEGATIVE:
				dialog.cancel();
				break;
				
			case DialogInterface.BUTTON_NEUTRAL:
				item_list.remove(position);
				adapter.notifyDataSetChanged();
				NewTableActivity.items.remove(name_prev);
				if(item_list.size() == 2){
					lockCallback.onLockUnlock(true, NewTableActivity.MASK_COMMIT_BUT);
				}
				break;
			}
			
		}
		
	}
	
	
}
