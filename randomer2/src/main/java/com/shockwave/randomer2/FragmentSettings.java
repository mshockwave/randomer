package com.shockwave.randomer2;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.support.v4.preference.PreferenceFragment;
import android.text.InputFilter;
import android.text.InputType;

public class FragmentSettings extends PreferenceFragment implements Main.BackButtonListener, SharedPreferences.OnSharedPreferenceChangeListener{
	static String LAP_COUNT_KEY = Common.PrefKeys.POINTER_LAP_COUNT;
	static String SPEED_KEY = Common.PrefKeys.POINTER_SPEED;
    static String SPIN_T_KEY = Common.PrefKeys.POINTER_SPIN_T;
	
	EditTextPreference pointer_lap_count;
    final int LAP_CNT_LENGTH = 3;
	ListPreference pointer_speed, pointer_spin_type;

	String current_lap_count = Common.PointerProp.lap_count_default;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		((Main)getActivity()).setBackButtonListener(this);
		
		//Set UI
		this.addPreferencesFromResource(R.layout.fragment_settings);
		
		onPrefChanged onprefchg = new onPrefChanged();
		//Init UI
		pointer_lap_count = (EditTextPreference)this.findPreference(LAP_COUNT_KEY);
		pointer_lap_count.getEditText().setInputType(InputType.TYPE_CLASS_NUMBER);
        pointer_lap_count.getEditText().setFilters(new InputFilter[]{new InputFilter.LengthFilter(LAP_CNT_LENGTH)});
		pointer_lap_count.setTitle(R.string.random_pointer_lap_count);
		pointer_lap_count.setOnPreferenceChangeListener(onprefchg);

        pointer_spin_type = (ListPreference)this.findPreference(SPIN_T_KEY);
        pointer_spin_type.setTitle(R.string.random_pointer_spin_t);
        pointer_spin_type.setEntries(R.array.pointer_spin_t);
        CharSequence[] type_entry_values = new CharSequence[]{Common.PointerProp.spin_t_accelerate+"",
                                                              Common.PointerProp.spin_t_roulette+"",
                                                              Common.PointerProp.spin_t_overshoot+"",
                                                              Common.PointerProp.spin_t_linear+""};
        pointer_spin_type.setEntryValues(type_entry_values);
		
		pointer_speed = (ListPreference)this.findPreference(SPEED_KEY);
		pointer_speed.setTitle(R.string.random_pointer_speed);
		CharSequence[] speed_entries = new CharSequence[]{getResources().getString(R.string.speed_high),
										   getResources().getString(R.string.speed_medium), 
										   getResources().getString(R.string.speed_low)};
		CharSequence[] speed_entry_values = new CharSequence[]{Common.PointerProp.speed_high_drt+"",
												Common.PointerProp.speed_medium_drt+"", 
												Common.PointerProp.speed_low_drt+""};
		pointer_speed.setEntries(speed_entries);
		pointer_speed.setEntryValues(speed_entry_values);
		
		
	}
	
	protected class onPrefChanged implements Preference.OnPreferenceChangeListener{

		@Override
		public boolean onPreferenceChange(Preference preference, Object newValue) {
			if(preference.getKey().equals(LAP_COUNT_KEY)){
				if( newValue.equals("") ){
					return false;
				}	
			}
			
			return true;
		}
		
	}
	
	@Override
	public void onSharedPreferenceChanged(SharedPreferences shared_pref, String key) {
		if(key.equals(SPEED_KEY)){
			pointer_speed.setSummary(pointer_speed.getEntry());
		}else if(key.equals(LAP_COUNT_KEY)){
			current_lap_count = pointer_lap_count.getSharedPreferences().getString(LAP_COUNT_KEY, Common.PointerProp.lap_count_default);
			pointer_lap_count.setSummary( getResources().getString(R.string.random_pointer_lap_count_info)+current_lap_count );
		}else if(key.equals(SPIN_T_KEY)){
            String _current_type = pointer_spin_type.getSharedPreferences().getString(SPIN_T_KEY, "NAN");
            int current_type;

            try {
                current_type = Integer.valueOf(_current_type);
            }catch(Exception e){
                pointer_spin_type.setSummary(getResources().getString(R.string.random_pointer_spin_t_info) + "Error");
                return;
            }

            String type = "Error";
            switch(current_type){
                case Common.PointerProp.spin_t_accelerate:
                    type = getResources().getString(R.string.spin_accelerate);
                    break;
                case Common.PointerProp.spin_t_roulette:
                    type = getResources().getString(R.string.spin_roulette);
                    break;
                case Common.PointerProp.spin_t_overshoot:
                    type = getResources().getString(R.string.spin_overshoot);
                    break;
                case Common.PointerProp.spin_t_linear:
                    type = getResources().getString(R.string.spin_linear);
                    break;
            }
            pointer_spin_type.setSummary(getResources().getString(R.string.random_pointer_spin_t_info) + " " + type);

        }
	}
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		
		Common.DEBUG("Fragment Settings onAttach");
	}
	
	@Override
	public void onPause(){
		this.getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
		super.onPause();
		Common.DEBUG("Fragment Settings onPause");
	}
	
	@Override
	public void onStart(){
		super.onStart();
		Common.DEBUG("Fragment Settings onStart");
	}
	
	@Override
	public void onResume(){
		super.onResume();
		this.getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
		Common.DEBUG("Fragment Settings onResume");
		
		//Set Default value
		if( pointer_lap_count.getSharedPreferences().getString(LAP_COUNT_KEY, "empty").equals("empty") ){
			pointer_lap_count.getSharedPreferences().edit().putString(LAP_COUNT_KEY, Common.PointerProp.lap_count_default).commit();
		}
		current_lap_count = pointer_lap_count.getSharedPreferences().getString(LAP_COUNT_KEY, Common.PointerProp.lap_count_default);
		pointer_lap_count.setSummary( getResources().getString(R.string.random_pointer_lap_count_info)+current_lap_count );
		
		if(pointer_speed.getValue() == null){
			pointer_speed.setValueIndex(1);//Default: Medium Speed
		}
		pointer_speed.setSummary(pointer_speed.getEntry());

        if(pointer_spin_type.getValue() == null){
            pointer_spin_type.setValueIndex(0);//Default: Accelerate
        }
        pointer_spin_type.setSummary(getResources().getString(R.string.random_pointer_spin_t_info) + " " + pointer_spin_type.getEntry());

	}
	
	@Override
	public void onDestroy(){
		super.onDestroy();
		Common.DEBUG("Fragment Settings onDestroy");
	}
	
	
	@Override
	public void BackButtonHandler() {
		((Main)getActivity()).selectTab(0, null);
	}

	
}
