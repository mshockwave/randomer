package com.shockwave.randomer2;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.SimpleAdapter;
import android.widget.Toast;

public class FragmentTable extends Fragment implements Main.BackButtonListener{

	List<String> item_list_name = new ArrayList<String>();
	ArrayList<HashMap<String, Object>> item_list = new ArrayList<HashMap<String,Object>>();
	List<Boolean> item_check_state = new ArrayList<Boolean>();
	boolean select_mode = false, is_all_selected = false;
	
	ListView table_list;
	RelativeLayout top_new;
	LinearLayout top_options;
	Button but_start, but_select_toggle;
	//ArrayAdapter<String> adapter = null;
	SimpleAdapter adapter = null;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		Common.DEBUG("Fragment Table onCreate");
		
	}
	
	@Override
	public void onAttach(Activity activity){
		super.onAttach(activity);
		Common.DEBUG("Fragment Table onAttach");
	}
	
	@Override
	public void onPause(){
		super.onPause();
		Common.DEBUG("Fragment Table onPause");
	}
	
	@Override
	public void onStart(){
		super.onStart();
		Common.DEBUG("Fragment Table onStart");
	}
	
	@Override
	public void onResume(){
		super.onResume();
		Common.DEBUG("Fragment Table onResume");
		
		String[] result;
		select_mode = false;
		replaceView(true);
		item_check_state.clear();
		if((result = Common.db.getTableNameArray()) != null){
			HashMap<String, Object> item;
			
			item_list_name.clear();
			item_list.clear();
			
			for(int i = 0 ; i < result.length ; i ++){
				item_list_name.add(result[i]);
				
				item = new HashMap<String, Object>();
				
				item.put("edit", R.drawable.ic_menu_edit);
				item.put("text", result[i]);
				item_list.add(item);
				
				item_check_state.add(false);
			}
			
			adapter.notifyDataSetChanged();
		}
		
	}
	
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
		View this_view = inflater.inflate(R.layout.fragment_table, null);
		
		Common.DEBUG("Fragment Table onCreateView");
		
		//UI part
		table_list = (ListView)this_view.findViewById(R.id.fragment_table_listview);
		top_new  = (RelativeLayout)this_view.findViewById(R.id.add_new_table);
		top_options = (LinearLayout)this_view.findViewById(R.id.options_table);
		top_options.setVisibility(View.INVISIBLE);
		but_start = (Button)this_view.findViewById(R.id.fragment_table_top_start);
		but_select_toggle = (Button)this_view.findViewById(R.id.fragment_table_top_choose);
		((Main)this.getActivity()).setBackButtonListener(this);
		
		adapter = ( new SimpleAdapter( getActivity().getBaseContext(), 
										item_list, 
										R.layout.list_item_table, 
										new String[]{"text", "edit"}, 
										new int[]{R.id.list_item_table_text, R.id.list_item_table_edit}){
						@Override
						public View getView(int position, View convertView, ViewGroup parent){
							View v = super.getView(position, convertView, parent);
							ImageView edit_but = (ImageView)v.findViewById(R.id.list_item_table_edit);
							
							if( item_check_state.get(position) ){
								v.setBackgroundColor(getResources().getColor(R.color.light_purple));
								edit_but.setOnClickListener(null);
							}else{
								v.setBackgroundColor(getResources().getColor(android.R.color.transparent));
								EditClick onEditClick = new EditClick(position);
								edit_but.setOnClickListener(onEditClick);
							}
							
							return v;
						}
						
					});
		
		
		ItemClick onitemclick = new ItemClick();
		table_list.setAdapter( adapter );
		table_list.setOnItemClickListener(onitemclick);
		table_list.setOnItemLongClickListener(onitemclick);
		
		top_new.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				Intent add = new Intent(getActivity().getBaseContext(), NewTableActivity.class);
				startActivity(add);
			}
		});
		
		but_start.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				
				ArrayList<String> tmp = new ArrayList<String>();
				boolean no_select = true;
				for(int i = 0 ; i < item_check_state.size() ; i++){
					if(item_check_state.get(i)){
						no_select = false;
						tmp.add(item_list_name.get(i));
					}
				}
				if(no_select){
					Toast.makeText(getActivity().getBaseContext(), R.string.no_table_select, Toast.LENGTH_SHORT).show();
					return;
				}
				
				Intent start = new Intent(getActivity().getBaseContext(), ShowActivity.class);
				start.putStringArrayListExtra("table_name", tmp);
				startActivity(start);
			}
		});
		
		but_select_toggle.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if( is_all_selected ){
					select_toggle(false);
				}else{
					select_toggle(true);
				}
			}
		});
		
		return this_view;
	}
	
	protected class EditClick implements View.OnClickListener{
		int position;
		
		public EditClick(int _position){
			this.position = _position;
		}
		
		@Override
		public void onClick(View v) {
			Bundle data = new Bundle();
			data.putString("table_name", item_list_name.get(position));
			
			((Main)getActivity()).selectTab(1, data);
		}
		
	}

	protected class ItemClick implements AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener, DialogInterface.OnClickListener{
		int click_id = 0;
		EditText rename_field = null;
		
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position, long id){
			
			if(select_mode){
				if( item_check_state.get((int)id) ){//prev checked
					item_check_state.set((int)id, false);
					adapter.notifyDataSetChanged();
					setSelectState(false);
				}else{
					item_check_state.set((int)id, true);
					adapter.notifyDataSetChanged();
					
				}
				//Common.DEBUG("checked: "+Integer.toBinaryString(item_check_state));
			}else{
				ArrayList<String> tmp = new ArrayList<String>();
				tmp.add( item_list_name.get(position) );
				
				Intent start = new Intent(getActivity().getBaseContext(), ShowActivity.class);
				start.putStringArrayListExtra("table_name", tmp);
				startActivity(start);
			}
			
		}

		@Override
		public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
			
			if( ! select_mode ){
				click_id = (int)id;
				AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());
				dialog.setItems(R.array.table_operate, this);
				dialog.create().show();
			}
			
			return true;
		}

		@Override
		public void onClick(DialogInterface dialog, int which) {
			switch(which){
			case 0: //Multi Select
				select_mode = true;
				replaceView(false);
				
				item_check_state.set(click_id, true);
				adapter.notifyDataSetChanged();
				break;
			
			case 1: //Rename
				rename_field = new EditText(getActivity().getBaseContext());
				rename_field.setLayoutParams( new ViewGroup.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT) );
				rename_field.setFilters( new InputFilter[]{new InputFilter.LengthFilter(20)} );
				rename_field.setTextColor(getResources().getColor(android.R.color.black));
				rename_field.setText(item_list_name.get(click_id));
				
				AlertDialog.Builder rename_dialog = new AlertDialog.Builder(getActivity());
				rename_dialog.setTitle(R.string.operate_rename);
				rename_dialog.setView(rename_field);
				rename_dialog.setPositiveButton(R.string.dialog_ok, this);
				rename_dialog.setNegativeButton(R.string.dialog_cancel, this);
				rename_dialog.create().show();
				
				break;
			
			case 2: //Remove
				rename_field = null;
				AlertDialog.Builder confirm = new AlertDialog.Builder(getActivity());
				String msg = getResources().getString(R.string.operate_remove)+" "+item_list_name.get(click_id)+"?";
				confirm.setMessage(msg);
				confirm.setNegativeButton(R.string.dialog_cancel, this);
				confirm.setPositiveButton(R.string.dialog_ok, this);
				confirm.create().show();
				break;
				
			case DialogInterface.BUTTON_POSITIVE:
				if(rename_field != null){//rename mode
					String after = (rename_field.getText()+"").trim();
					if(after.equals("")){
						dialog.cancel();
						break;
					}
					
					ContentValues tables = Common.db.getTableName();
					
					if(tables.containsKey(after)){
						Toast.makeText(getActivity(), R.string._new_table_name_error, Toast.LENGTH_SHORT).show();
						dialog.cancel();
						break;
					}else{
						try{
							Common.db.rename_table( Common.db.getIndex(item_list_name.get(click_id)), after);
						}catch(Exception e){
							Common.DEBUG("Rename table fail");
							break;
						}
						item_list_name.set(click_id, after);
						
						HashMap<String, Object> new_obj = new HashMap<String, Object>();
						new_obj.put("text", after);
						new_obj.put("edit", R.drawable.ic_menu_edit);
						item_list.remove(click_id);
						item_list.add(click_id, new_obj);
						
						adapter.notifyDataSetChanged();
					}
				}else{//remove mode
					try{
						Common.db.remove_table( Common.db.getIndex( item_list_name.get(click_id) ) );
					}catch(Exception e){
						Common.DEBUG("Remove table fail");
						break;
					}
					item_list_name.remove(click_id);
					item_list.remove(click_id);
					adapter.notifyDataSetChanged();
				}
				break;
			case DialogInterface.BUTTON_NEGATIVE:
				dialog.cancel();
				break;
			}
			
		}
	}

	private void replaceView(boolean isnew){
		if(isnew){
			top_new.setVisibility(View.VISIBLE);
			top_options.setVisibility(View.INVISIBLE);
		}else{
			top_new.setVisibility(View.INVISIBLE);
			top_options.setVisibility(View.VISIBLE);
		}
	}
	
	private void select_toggle(boolean selectall){
		int count = table_list.getCount();
		
		if(selectall){
			for(int i = 0 ; i < count ; i++){
				item_check_state.set( i, true);
			}
			adapter.notifyDataSetChanged();
			setSelectState(true);
		}else{
			for(int i = 0 ; i < count ; i++){
				item_check_state.set( i, false);
			}
			adapter.notifyDataSetChanged();
			setSelectState(false);
		}
		
		//Common.DEBUG("state: "+Integer.toBinaryString(item_check_state));
		
	}
	
	private void setSelectState(boolean state){ 
		is_all_selected = state;
		
		if(state){
			but_select_toggle.setText(R.string.deselect_all);
		}else{
			but_select_toggle.setText(R.string.select_all);
		}
	}
	
	@Override
	public void BackButtonHandler() {
		
		if(!select_mode){
			((Main)this.getActivity()).setBackButtonListener(null);
		}else{
			replaceView(true);
			select_mode = false;
			select_toggle(false);
		}
	}
}
