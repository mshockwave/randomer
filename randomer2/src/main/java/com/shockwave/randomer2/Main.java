package com.shockwave.randomer2;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBarActivity;

public class Main extends ActionBarActivity{
	
	public static SharedPreferences app_settings;
	ActionBar actionbar;
	BackButtonListener backbutton = null;
	Bundle fragment_extra = null;
	
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		//Init Part
		Common.AppProp.screen_d = getResources().getDisplayMetrics().density;
        Common.AppProp.height_px = getResources().getDisplayMetrics().heightPixels;
        Common.AppProp.width_px = getResources().getDisplayMetrics().widthPixels;
        
        Common.FrameProp.padding_all = getResources().getDimension(R.dimen.FrameLayout_main_padding);//Already pixels, no need to cast
        
        app_settings = PreferenceManager.getDefaultSharedPreferences(this);
        
        //Common.AppProp.first_time = app_settings.getBoolean("first_time", true);
        
        Common.db = new DBHelper(getBaseContext());//init the database
        
        //UI part
        this.setContentView(R.layout.activity_main);
        
        //ActionBar Part
        actionbar = this.getSupportActionBar();
        actionbar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
        ActionBar.TabListener tabListener = new ActionBar.TabListener() {
			@Override
			public void onTabReselected(Tab arg0, FragmentTransaction arg1) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void onTabSelected(Tab tab, FragmentTransaction ftr) {
				Fragment fragment = null;
				
				switch(tab.getPosition()){
				case 0: //Tables
					setTitle(R.string.app_name);
					fragment = new FragmentTable();
					break;
				case 1: //Items
					fragment = new FragmentItem();
					break;
				case 2: //Settings
					fragment = new FragmentSettings();
					break;
				}
				
				if(fragment != null){
					if(fragment_extra != null){
						fragment.setArguments(fragment_extra);
					}
					ftr.replace(R.id.Frame_replaced, fragment);
					//ftr.commit(); //IMPORTANT!! DO NOT CALL COMMIT!!
				}
			}

			@Override
			public void onTabUnselected(Tab arg0, FragmentTransaction arg1) {
				//pass
			}
        };
        
        
        final String[] tab_text = new String[]{getResources().getString(R.string.actionbar_text_table),
        								 	   getResources().getString(R.string.actionbar_text_detail),
        								 	   getResources().getString(R.string.actionbar_text_setting)};
        
        for (String text : tab_text) {
            actionbar.addTab(
                    actionbar.newTab()
                            .setText(text)
                            .setTabListener(tabListener));
        }
        
        
	}
	
	
	public interface BackButtonListener{
		public void BackButtonHandler();
	}
	
	public void setBackButtonListener(BackButtonListener bl){
		this.backbutton = bl;
	}
	
	public void selectTab(int index, Bundle extra){
		this.fragment_extra = extra;
		actionbar.selectTab(actionbar.getTabAt(index));
	}
	
	@Override
	public void onBackPressed(){
		if(backbutton != null){
			backbutton.BackButtonHandler();
			if(backbutton == null)//check again
				super.onBackPressed();
			
		}else{
			super.onBackPressed();
		}
	}
	
	@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        
        return true;
    }
    
}
