package com.shockwave.randomer2;

import java.util.Iterator;
import java.util.Map.Entry;
//import java.util.Set;

import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class NewTableActivity extends FragmentActivity implements FragmentNewUser.LockUnlockListener, FragmentNewNumber.LockUnlockListener{
	
	EditText edit_name;
	TextView text_error;
	Button but_type,but_commit,but_cancel;
	
	public static final int MASK_ERROR_TEXT = (1 << 0);
	public static final int MASK_TYPE_BUT = (1 << 1);
	public static final int MASK_COMMIT_BUT = (1 << 2);
	
	ContentValues table_name;
	public static ContentValues items = null;
	boolean hasitem = false;
	
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		
		//UI part
		this.setContentView(R.layout.activity_new_table);
		edit_name = (EditText)findViewById(R.id._table_name_edit);
		text_error = (TextView)findViewById(R.id._table_name_error);
		but_type = (Button)findViewById(R.id._table_type_select);
		but_commit = (Button)findViewById(R.id._table_but_commit);
		but_cancel = (Button)findViewById(R.id._table_but_cancel);
		
		//Get all the tables' name
		table_name = Common.db.getTableName();
		
		items = new ContentValues();
		
		NameChecker nk = new NameChecker();
		edit_name.addTextChangedListener(nk);
		ChooseType tl = new ChooseType();
		but_type.setOnClickListener(tl);
		DoneDiscardClick cl = new DoneDiscardClick();
		but_commit.setOnClickListener(cl);
		but_cancel.setOnClickListener(cl);
	}
	
	private boolean check(String name){
		if( table_name.getAsByte(name) == null ){
			return true;
		}else{			
			return false;
		}
	}
	
	public void lock(int bit_mask){
		
		if( (bit_mask & MASK_ERROR_TEXT) != 0 ){
			text_error.setVisibility(View.VISIBLE);
		}
		
		if( (bit_mask & MASK_TYPE_BUT) != 0){
			but_type.setEnabled(false);
		}
		
		if( (bit_mask & MASK_COMMIT_BUT) != 0 ){
			but_commit.setEnabled(false);
		}
		
	}
	
	public void unlock(int bit_mask){
		if( (bit_mask & MASK_ERROR_TEXT) != 0 ){
			text_error.setVisibility(View.INVISIBLE);
		}
		
		if( (bit_mask & MASK_TYPE_BUT) != 0){
			but_type.setEnabled(true);
		}
		
		if( (bit_mask & MASK_COMMIT_BUT) != 0 ){
			but_commit.setEnabled(true);
		}
	}
	
	@Override
	public void onLockUnlock(boolean lock, int bit_mask) {
		if( lock ){
			hasitem = false;
			lock(bit_mask);
		}else{
			hasitem = true;
			unlock(bit_mask);
		}
		
	}
	
	protected class NameChecker implements TextWatcher{

		@Override
		public void afterTextChanged(Editable s) {
			if( (""+s).trim().equals("") ){
				lock(MASK_TYPE_BUT | MASK_COMMIT_BUT);
				return;
			}
			
			if( check( (""+s).trim() ) ){
				unlock(MASK_ERROR_TEXT | MASK_TYPE_BUT);
				if(hasitem)
					unlock(MASK_COMMIT_BUT);
			}else{
				lock(MASK_ERROR_TEXT | MASK_TYPE_BUT | MASK_COMMIT_BUT);
			}
			
		}

		@Override
		public void beforeTextChanged(CharSequence s, int start, int count,
				int after) {
			
			
		}

		@Override
		public void onTextChanged(CharSequence s, int start, int before,
				int count) {
			
			
		}
		
	}

	protected class ChooseType implements View.OnClickListener, DialogInterface.OnClickListener{

		AlertDialog.Builder dialog = new AlertDialog.Builder(NewTableActivity.this);
		FragmentTransaction ft;
		Fragment fragment = null;
		
		//Choose type onclick
		@Override
		public void onClick(View v) {
			dialog.setTitle(R.string._new_table_type_default).setItems(R.array.table_type, this);
			
			dialog.create().show();
		}

		//Dialog onclick
		@Override
		public void onClick(DialogInterface dialog, int which) {
			ft = getSupportFragmentManager().beginTransaction();
			
			switch(which){
			case 0:
				fragment = new FragmentNewUser();
				break;
			case 1:
				fragment = new FragmentNewNumber();
				break;
			}
			items.clear();
			ft.replace(R.id._table_fragment_replaced, fragment);
			ft.commit();
		}
		
	}
	
	protected class DoneDiscardClick implements View.OnClickListener{

		@Override
		public void onClick(View v) {
			switch(v.getId()){
			case R.id._table_but_commit:
				if(items.size() == 0){
					NewTableActivity.this.finish();
					break;
				}
				
				String table_final = edit_name.getText()+"";
				try {
					Common.db.create_table(table_final);
				} catch (Exception e) {
					Common.DEBUG("Create "+table_final+" fail");
					NewTableActivity.this.finish();
				}
				
				int table_index = 0;
				try {
					table_index = Common.db.getIndex(table_final);
				} catch (Exception e) {
					Common.DEBUG("get "+table_final+" index fail");
					NewTableActivity.this.finish();
				}
				Iterator<Entry<String, Object >> it = items.valueSet().iterator();
				
				Entry<String, Object> tmp_obj;
				while( it.hasNext() ){
					tmp_obj = it.next();
					try {
						Common.db.insert_new(table_index, tmp_obj.getKey(), (Integer)tmp_obj.getValue());
						Common.DEBUG("Name: "+tmp_obj.getKey());
						Common.DEBUG("Weight: "+(Integer)tmp_obj.getValue());
					} catch (Exception e) {
						Common.DEBUG("insert "+table_final+" value fail");
						NewTableActivity.this.finish();
					}
				}
				NewTableActivity.this.finish();
				break;
			case R.id._table_but_cancel:
				NewTableActivity.this.finish();
				break;
			}
			
		}
		
	}
	

}
