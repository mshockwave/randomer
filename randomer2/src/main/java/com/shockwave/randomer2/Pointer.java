package com.shockwave.randomer2;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.OvershootInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class Pointer extends ImageView implements Animation.AnimationListener{

    SharedPreferences settings;
	SharedPreferences.Editor settings_edit;

    String spin_type;

	onSpinFinishListener mSpinFinish = null;

	public Pointer(Context context, AttributeSet attrs) {
		super(context, attrs);
		Common.DEBUG("Pointer construct");

		settings = Main.app_settings;
		spin_type = settings.getString(Common.PrefKeys.POINTER_SPIN_T, Common.PointerProp.spin_t_accelerate+"");//default: accelerate
	}
	
	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec){
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		Common.DEBUG("Pointer onmeasure");

	}
	
	@Override
	protected void onDraw(Canvas canvas){
		super.onDraw(canvas);
		Common.DEBUG("Pointer ondraw");
		
		set_image();

	}
	
	
	/*
	 * Handling the job of resizing and putting pointer img in correct position*/
	public final void set_image(){
		
		FrameLayout.LayoutParams lp = null;
        int imgh, imgw;
		
		if( ! settings.getBoolean("pointer_prop_cached", false) ){
			
			//Resize the image
			imgh = this.getHeight();
			imgw = this.getWidth();
			Common.DEBUG("img_main_h_before: "+imgh+", img_main_w_before: "+imgw);
			float crossLine = (float)Math.sqrt( Math.pow(imgh, 2) + Math.pow(imgw, 2) );
			float percent = ( ( Common.RingProp.radius_in_px * 2 ) / crossLine );
			imgw = (int)(imgw * percent);
			imgh = (int)(imgh * percent);
			
			//set the image position
			lp = new FrameLayout.LayoutParams( imgw, imgh );
			float margin_l_r = ( Common.RingProp.view_w - imgw ) / 2;
			float margin_u_b = ( Common.RingProp.view_h - imgh ) / 2;
			lp.gravity = Gravity.TOP;
			lp.setMargins( (int)margin_l_r, (int)margin_u_b, (int)margin_l_r, (int)margin_u_b );
			
			settings_edit = settings.edit();
			settings_edit.putInt("pointer_imgw", imgw);
			settings_edit.putInt("pointer_imgh", imgh);
			settings_edit.putFloat("pointer_margin_l_r", margin_l_r);
			settings_edit.putFloat("pointer_margin_u_b", margin_u_b);
			settings_edit.putBoolean("pointer_prop_cached", true);
			
			settings_edit.commit();
			
		}else{
            imgw = settings.getInt("pointer_imgw", 0);
            imgh = settings.getInt("pointer_imgh", 0);
			lp = new FrameLayout.LayoutParams(imgw , imgh );
			float margin_l_r = settings.getFloat("pointer_margin_l_r", 0.0f);
			float margin_u_b = settings.getFloat("pointer_margin_u_b", 0.0f);
			lp.gravity = Gravity.TOP;
			lp.setMargins( (int)margin_l_r, (int)margin_u_b, (int)margin_l_r, (int)margin_u_b );
		}

        Common.DEBUG("lp w: "+lp.width+", lp h: "+lp.height);
		this.setLayoutParams(lp);
		Common.DEBUG("Radius*2 in: "+Common.RingProp.radius_in_px*2);
		Common.DEBUG("img_main_h_after: "+this.getHeight()+", img_main_w_after: "+this.getWidth());
	}
	
	int start_dg = 0;
	public int spin(Random rand, ArrayList<Integer> degree_set, int count, int duration){
		RotateAnimation main_anime;
		Interpolator inpl;
        int value_spin_type = Integer.valueOf(spin_type);
        int degree = 0;

        Collections.shuffle(degree_set);
        degree = degree_set.get( (degree_set.size()-1) - rand.nextInt(degree_set.size()) );

        inpl = new AccelerateDecelerateInterpolator();//default
        switch(value_spin_type){
            case Common.PointerProp.spin_t_accelerate:
                break;
            case Common.PointerProp.spin_t_roulette:
                inpl = new DecelerateInterpolator();
                break;
            case Common.PointerProp.spin_t_overshoot:
                inpl = new OvershootInterpolator( 1.0f + rand.nextFloat() );
                break;
            case Common.PointerProp.spin_t_linear:
                inpl = new LinearInterpolator();
                break;
        }

		main_anime = new RotateAnimation(start_dg, start_dg + degree + 360 * count, 
										 Animation.RELATIVE_TO_SELF, 0.5f, 
										 Animation.RELATIVE_TO_SELF, 0.5f);
		main_anime.setInterpolator(inpl);
		main_anime.setDuration( duration * (degree + 360 * count) );
		main_anime.setFillAfter(true);
		main_anime.setAnimationListener(this);
		
		this.startAnimation(main_anime);
		
		start_dg += degree;
		start_dg %= 360;
		
		return start_dg;
	}
	
	public interface onSpinFinishListener{
		public void onSpinFinish();
	}
	
	public void setOnSpinFinish(onSpinFinishListener spin_l){
		this.mSpinFinish = spin_l;
	}

	@Override
	public void onAnimationEnd(Animation animation) {
		mSpinFinish.onSpinFinish();
	}

	@Override
	public void onAnimationRepeat(Animation animation) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onAnimationStart(Animation animation) {
		// TODO Auto-generated method stub
		
	}

}
