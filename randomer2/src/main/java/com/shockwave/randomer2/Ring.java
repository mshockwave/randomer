package com.shockwave.randomer2;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class Ring extends View{
	RectF oval = null;
	Rect cache_rect = null;
	Bitmap cache_bm = null;
	Paint paint = null;
	SharedPreferences settings;
	SharedPreferences.Editor settings_edit;
	
	long weight_sum = 0;
	
	int finalW,finalH;
	private final float strokeWidth = 40.0f;//Unit: dip
	ArrayList<Integer> color_set = new ArrayList<Integer>();
	ArrayList<Float> angle_avoid = new ArrayList<Float>();
	
	ColorSetFinish colorfinishcb = null;
	
	public Ring(Context context, AttributeSet attrs) {
		super(context, attrs);
		Common.DEBUG("Ring Construct");
		
		paint = new Paint();
		oval = new RectF();
		cache_rect = new Rect();
		
		settings = Main.app_settings;
		
		//putColor();
		
		for(int w : ShowActivity.item_weight_list){
			weight_sum += w;
		}
		Common.DEBUG("weight sum: "+weight_sum);
		
		
	}
	
	private void putColor(){
		if(ShowActivity.item_name_list != null){
			int count = ShowActivity.item_name_list.size();
			Set<Integer> color_random = new HashSet<Integer>();
			Random r = new Random();
			
			while(color_random.size() < count){
				color_random.add( Color.rgb(r.nextInt(256), 
										   	r.nextInt(256), 
										   	r.nextInt(256)) );
			}
			
			Iterator<Integer> it = color_random.iterator();
			while(it.hasNext()){
				color_set.add(it.next());
			}
		}else{
			color_set.add(Color.BLACK);
		}
		
		this.colorfinishcb.onColorSetFinish(color_set);
	}
	
	public interface ColorSetFinish{
    	public void onColorSetFinish(ArrayList<Integer> color_set);
    }
	
	public void setColorSetFinish(ColorSetFinish color_finish){
		this.colorfinishcb = color_finish;
	}
	
	//Unit: dip
	static final float DESIRED_LENGTH_BEFORE_PADDING = 200;

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

	   Common.DEBUG("Ring onmeasure");
	   Common.DEBUG("Density: "+Common.AppProp.screen_d);
	   
	   int w = 0, h = 0;
	   
	   if( ! settings.getBoolean("ring_prop_cached", false) ){
		   
		   int wDesired = (int) (getPaddingLeft() + getPaddingRight() + 
		   			Math.max(Common.CastToPx(DESIRED_LENGTH_BEFORE_PADDING), getSuggestedMinimumWidth()));
		   int wSpecMode = MeasureSpec.getMode(widthMeasureSpec);
		   w = 0;
		   switch(wSpecMode){
		   case MeasureSpec.EXACTLY:
			   w = MeasureSpec.getSize(widthMeasureSpec);
			   Common.DEBUG("wmode: exactly");
			   break;
		   case MeasureSpec.AT_MOST:
			   w = Math.min(wDesired, MeasureSpec.getSize(widthMeasureSpec));
			   Common.DEBUG("wmode: at_most");
			   break;
		   case MeasureSpec.UNSPECIFIED:
			   Common.DEBUG("wmode: unspecified");
			   w = wDesired;
			   break;
		   }	
		   
		   
		   int hSpecMode = MeasureSpec.getMode(heightMeasureSpec);
		   h = 0;
		   switch(hSpecMode){
		   case MeasureSpec.EXACTLY:
			   h = MeasureSpec.getSize(heightMeasureSpec);
			   Common.DEBUG("hmode: exactly");
			   break;
		   case MeasureSpec.AT_MOST:
			   h = Math.min(w, MeasureSpec.getSize(heightMeasureSpec));
			   Common.DEBUG("hmode: at_most");
			   break;
		   case MeasureSpec.UNSPECIFIED:
			   Common.DEBUG("hmode: unspecified");
			   h = w;
			   break;
		   }
		   
		   settings_edit = settings.edit();
		   settings_edit.putInt("ring_view_w", w);
		   settings_edit.putInt("ring_view_h", h);
		   settings_edit.putBoolean("ring_prop_cached", true);
		   settings_edit.commit();
		   
	   	}else{
		   w = settings.getInt("ring_view_w", 0);
		   h = settings.getInt("ring_view_h", 0);
	   	}
		
	   

	   Common.DEBUG("finalW: "+w+", finalH: "+h);
	   finalW = w;
	   finalH = h;
	   Common.RingProp.view_h = finalH;
	   Common.RingProp.view_w = finalW;
	   setMeasuredDimension(w, h);
	   
	   
	   if(cache_bm == null){
		   this.setDrawingCacheEnabled(true);
	       cache_bm = this.getDrawingCache();
	   }
	   //Log.d("randomer_debug","imgh: "+MainActivity.img.getHeight()+", imgw: "+MainActivity.img.getWidth());
	}
	
	
	@Override
    protected void onDraw(Canvas canvas){
        super.onDraw(canvas);
        Common.DEBUG("Ring ondraw");

        if(cache_bm != null){
        	cache_rect.left = cache_rect.top = 0;
        	cache_rect.right = cache_bm.getWidth();
        	cache_rect.bottom = cache_bm.getHeight();
        	
        	Common.DEBUG("Ring: use cache to draw");
        	canvas.drawBitmap(cache_bm, cache_rect, cache_rect, null);
        	return;
        }
        
        Common.DEBUG("Ring: use normal way to draw");
        canvas.drawColor(Color.TRANSPARENT);
        
        putColor();
        angle_avoid.clear();
        angle_avoid.add(0.0f);
        RingDraw(270.0f, color_set, canvas);
        
       /*paint.setColor(Color.GRAY);
        *paint.setStrokeWidth(1.0f);
        *canvas.drawLine(0 + Common.CastToPx(strokeWidth), finalH / 2, finalW - Common.CastToPx(strokeWidth), finalH / 2, paint);*/
        
        //Log.d("randomer_debug","imgh: "+MainActivity.img.getHeight()+", imgw: "+MainActivity.img.getWidth());
    
        
    }
	
	/*
	 * The main draw ring method
	 * @param startAngle: The start angle of the colors
	 * @param colorSet: 	 The color set, which elements are type of class Color
	 * @param canvas:		 The main canvas*/
	private final void RingDraw(float startAngle, ArrayList<Integer> colorSet, Canvas canvas){
		
		float offset = Common.CastToPx(strokeWidth) / 2;
		float sweepAngle = 0f;
		
		Common.RingProp.strokeoffset_px = offset;
		
		oval.set(0 + offset, 0 + offset, finalW - offset, finalH - offset);//Left, Top, Right, Bottom
		Common.RingProp.center_x = oval.centerX();
		Common.RingProp.center_y = oval.centerY();
		Common.RingProp.radius_out_px = (float)( oval.right - oval.left ) / 2 + offset;
		Common.RingProp.radius_in_px = Common.RingProp.radius_out_px - offset * 2;
		
		paint.setAntiAlias(true);
		paint.setStrokeWidth(Common.CastToPx(strokeWidth));
        paint.setStyle(Style.STROKE);
       
        Iterator<Integer> it_color = colorSet.iterator();
        Iterator<Integer> it_weight = ShowActivity.item_weight_list.iterator();
        while(it_color.hasNext() && it_weight.hasNext() && weight_sum != 0){
    	    try{
    		    paint.setColor(it_color.next());
    	    }catch(Exception e){
    		    paint.setColor(Color.TRANSPARENT);
    	    }
    	    
    	    sweepAngle = (float)( 360 * (float)it_weight.next() / weight_sum );
    	    Common.DEBUG("angle: "+sweepAngle);
    	    
    	    canvas.drawArc(oval, startAngle, sweepAngle, false, paint);
    	    startAngle += sweepAngle;
    	    angle_avoid.add(startAngle - 270);
        }
        Common.DEBUG("angle avoid size: "+angle_avoid.size());
        
	}
	
	public ArrayList<Integer> getColorSet(){
		return this.color_set;
	}
	
	public ArrayList<Float> getAngleAvoid(){
		return this.angle_avoid;
	}

}
