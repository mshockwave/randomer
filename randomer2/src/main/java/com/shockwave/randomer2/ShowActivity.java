package com.shockwave.randomer2;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;

public class ShowActivity extends ActionBarActivity implements Ring.ColorSetFinish{
	
	public static ArrayList<String> item_name_list = null;
	public static ArrayList<Integer> item_weight_list = null;
	ArrayList<String> table_name_list;
	ArrayList<Integer> table_size_list, color_set = null;
	ArrayList<HashMap<String, Object>> color_list = new ArrayList<HashMap<String, Object>>();
	int item_result_id = 0;
    int pointer_lap_count, pointer_duration;
	boolean color_update_flag = true;
	
	Button but_start;
	Pointer arrow;
	Ring ring;
	
	DrawerLayout drawer;
	ListView drawer_color_list = null;
	SimpleAdapter list_adapter = null;
	LinearLayout drawer_layout;
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        pointer_lap_count = Integer.valueOf(Main.app_settings.getString(Common.PrefKeys.POINTER_LAP_COUNT, Common.PointerProp.lap_count_default));
        pointer_duration = Integer.valueOf(Main.app_settings.getString(Common.PrefKeys.POINTER_SPEED, Common.PointerProp.speed_medium_drt+""));

        table_name_list = this.getIntent().getStringArrayListExtra("table_name");
        Iterator<String> it_table_name = table_name_list.iterator();
        
        item_name_list = new ArrayList<String>();
    	item_weight_list = new ArrayList<Integer>();
    	table_size_list = new ArrayList<Integer>();
    	
        while(it_table_name.hasNext()){
        	String table_name = it_table_name.next();
        	Bundle result;
        	
        	try{
        		result = Common.db.query( Common.db.getIndex(table_name) );
        	}catch(Exception e){
        		Common.DEBUG("Error: no such table: "+table_name);
        		continue;
        	}
        	
        	Iterator<String> it_tmp_name = result.getStringArrayList("name").iterator();
        	Iterator<Integer> it_tmp_weight = result.getIntegerArrayList("weight").iterator();
        	if(it_tmp_name == null || it_tmp_weight == null) continue;
        	
        	String first_name = it_tmp_name.next();
        	int first_weight = it_tmp_weight.next();
        	
        	if(first_weight < 0){//sequence numbers
        		int step = 1;
        		ArrayList<Integer> edges = new ArrayList<Integer>();
        		
        		if(first_weight == -1){//handling first value pair
        			edges.add(Integer.valueOf(first_name));
        		}else{
        			Common.DEBUG("first was step");
        			try{	
        				Common.DEBUG("step substring: "+first_name.substring(1, first_name.length()-1));
        				step = Integer.valueOf( first_name.substring(1, first_name.length()-1) );
        			}catch(Exception e){
        				Common.DEBUG("step error");
        				step = 1;
        			}
        		}
        		
        		while(it_tmp_weight.hasNext() && it_tmp_name.hasNext()){//handling the rest two data
        			int tmp_weight = it_tmp_weight.next();
        			String tmp_name = it_tmp_name.next();
        			
    				if(tmp_weight == -1){
    					edges.add(Integer.valueOf(tmp_name));
    				}else{
    					try{	
    						Common.DEBUG("step substring: "+tmp_name.substring(1, tmp_name.length()-1));
	        				step = Integer.valueOf( tmp_name.substring(1, tmp_name.length()-1) );
	        			}catch(Exception e){
	        				Common.DEBUG("Fail catched");
	        				step = 1;
	        			}
    				}
    			}
        		
        		if(edges.get(0) > edges.get(1)){//edges[0]->start  edges[1]->end  edges[2]->step
        			int tmp = edges.get(0);
        			edges.set(0, edges.get(1));
        			edges.set(1, tmp);
        		}
        		edges.add(step);
        		
        		for( int i = edges.get(0) ; i <= edges.get(1) ; i += edges.get(2) ){
        			item_name_list.add(""+i);
        			item_weight_list.add(1);
        		}
        		Common.DEBUG("item_name_list count: "+item_name_list.size());
        		
        	}else{//user define
        		item_name_list.add(first_name);
        		item_weight_list.add(first_weight);
        		
        		while(it_tmp_name.hasNext() && it_tmp_weight.hasNext()){
        			item_name_list.add(it_tmp_name.next());
        			item_weight_list.add(it_tmp_weight.next());
        		}
        		Common.DEBUG("item_name_list count: "+item_name_list.size());
        	}
        	
        	table_size_list.add( item_name_list.size() );
        	
        }
        
        color_list_init();
        list_adapter = ( new SimpleAdapter(getBaseContext(), 
        								   color_list, 
        								   R.layout.list_item_drawer, 
        								   new String[]{"text"}, 
        								   new int[]{R.id.list_item_drawer_text}){
    		
    		@Override
    		public View getView(int position, View convertView, ViewGroup parent){
    			View v = super.getView(position, convertView, parent);
    			
    			Common.DEBUG("=============");
    			Integer color = (Integer) color_list.get(position).get("color");
    			
    			/*if(color.equals(Color.TRANSPARENT)){
    				Common.DEBUG("bg color: transparent");
    			}else{
    				Common.DEBUG("bg color: "+color);
    			}*/
    			v.setBackgroundColor(color);
    			
    			if( color.equals(Color.TRANSPARENT) ){
    				((TextView)v).setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
    				((TextView)v).setTextColor( Color.BLACK );
    				Common.DEBUG("color: text was set to center");
    			}else{
    				if( isColorBright(color) ){
    					Common.DEBUG("color: text set to black");
        				((TextView)v).setTextColor( Color.BLACK );
        			}else{
        				Common.DEBUG("color: text set to white");
        				((TextView)v).setTextColor( Color.WHITE );
        			}
    				((TextView)v).setGravity(Gravity.LEFT | Gravity.CENTER_VERTICAL);
    			}
    			
    			return v;
    		}
    	});
        
        //UI part
        LayoutInflater inflater = (LayoutInflater)this.getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View this_layout = inflater.inflate(R.layout.activity_show, null);
        
        ring = (Ring)this_layout.findViewById(R.id.Ring_main);
        ring.setColorSetFinish(this);
        
        this.setContentView(this_layout);
        
        but_start = (Button)this.findViewById(R.id.Button_start);
        but_start.setOnClickListener(new ButStartClick());
        arrow = (Pointer)this.findViewById(R.id.Pointer_main);
        SpinFinish sf = new SpinFinish();
        arrow.setOnSpinFinish(sf);
        
        drawer = (DrawerLayout)this.findViewById(R.id.Drawer_show);
        int drawer_width_new = (int)(Common.AppProp.width_px / 3 * 2);
        drawer_layout = (LinearLayout)this.findViewById(R.id.drawer_layout);
        drawer_layout.getLayoutParams().width = drawer_width_new;
        drawer_color_list = (ListView)this.findViewById(R.id.drawer_listview);
        drawer_color_list.getLayoutParams().width = drawer_width_new;
        drawer_color_list.setAdapter(list_adapter);
        
        
    }
    
    private void color_list_init(){
    	Iterator<String> it_table_name = table_name_list.iterator();
    	Iterator<Integer> it_table_size = table_size_list.iterator();
    	int size_current ;
    	if( !it_table_name.hasNext() || !it_table_size.hasNext() ) return;
    	
    	color_list.clear();
    	
    	HashMap<String, Object> item = new HashMap<String, Object>();
    	
    	//first table name
    	item.put("text", it_table_name.next());
    	item.put("color", Color.TRANSPARENT);
    	color_list.add(item);
    	
    	size_current = it_table_size.next();
    	for( int i = 0 ; i < item_name_list.size() ; i++ ){
    		item = new HashMap<String, Object>();
    		item.put("text", item_name_list.get(i));
    		item.put("color", Color.BLACK); //Do not set the real color first because ring hasn't drew yet
    		color_list.add(item);
    		
    		if( (i+1) == size_current ){//table name item
    			if( it_table_name.hasNext() && it_table_size.hasNext() ){
    				item = new HashMap<String, Object>();
    				item.put("text", it_table_name.next());
    				item.put("color", Color.TRANSPARENT);
    				
    				color_list.add(item);
    				
    				size_current = it_table_size.next();
    			}
    		}
    	}
    	
    	Common.DEBUG("color_list size: "+color_list.size());
    }
    
    private boolean isColorBright(int color){
		float red_p = (float)Color.red(color) / 255f;
		float green_p = (float)Color.green(color) / 255f;
		float blue_p = (float)Color.blue(color) / 255f;
		float percent;
		
		percent = (float)( red_p * 0.299 + green_p * 0.587 + blue_p * 0.111 );

        return percent > 0.5f;
	}
    
    private String getItemParent(int item_id){
    	int item_index = item_id;
    	if(item_index < 0) item_index = 0;
    	int table_index = 0;
    	
    	item_index += 1;
    	
    	for(int i = 0 ; i < table_size_list.size() ; i++){
    		if( item_index > table_size_list.get(i) ){
    			table_index++;
    		}else{
    			break;
    		}
    	}
    	
    	return table_name_list.get(table_index);
    }
    
    protected class DialogShow implements DialogInterface.OnClickListener{
    	int item_id;
    	String item_parent;
    	int item_color;
    	
    	AlertDialog.Builder dialog;
    	View dialog_view;
    	
    	public void setResult(int id){
    		item_id = id;
    		item_parent = getItemParent( item_id );
    		
    		dialog = new AlertDialog.Builder(ShowActivity.this);
    		dialog.setTitle(item_parent);
    		dialog.setPositiveButton(getResources().getString(R.string.dialog_ok), this);
    		
    		dialog_view = getLayoutInflater().inflate(R.layout.dialog_random_result, null);
    		
    		try{	
    			item_color = color_set.get(item_id);
    		}catch(Exception e){
    			item_color = Color.TRANSPARENT;
    		}
    		
    		this.setDialogView();
    		
    		dialog.setView(dialog_view);
    		
    	}
    	
    	
    	
    	private void setDialogView(){
    		TextView text = (TextView)dialog_view.findViewById(R.id.dialog_result_text);
    		text.setText( item_name_list.get(item_id) );
    		if( isColorBright(item_color) ){
    			text.setTextColor(Color.BLACK);
    		}else{
    			text.setTextColor(Color.WHITE);
    		}
    		
    		dialog_view.setBackgroundColor(item_color);
    	}
    	
    	public void show(){
    		dialog.create().show();
    	}
    	
		@Override
		public void onClick(DialogInterface dialog, int which) {
			if(which == DialogInterface.BUTTON_POSITIVE)
				dialog.cancel();
		}
    	
    }
    
    protected class SpinFinish implements Pointer.onSpinFinishListener{
    	DialogShow dialog = new DialogShow();
    	
		@Override
		public void onSpinFinish() {
			dialog.setResult(item_result_id);
			dialog.show();
		}
    	
    }
    
    protected class ButStartClick implements View.OnClickListener{

    	int angle_after = 0;
        boolean is_empty = false;
    	ArrayList<Integer> random_degree_set;
        ArrayList<Float> angle_list;

    	public ButStartClick(){
            angle_list = ring.getAngleAvoid();

            if(angle_list.size() == 1 && angle_list.get(0) == 0.0f) {
                is_empty = true; //weight sum == 0
            }else {
                is_empty = false;
                random_degree_set = new ArrayList<Integer>();

                for (int i = 0; i < 360; i++) {
                    if( !angle_list.contains((float)i) )
                        random_degree_set.add(i);
                }

            }
    	}
    	
		@Override
		public void onClick(View v) {

			if(is_empty){
                Toast.makeText(getBaseContext(), R.string.error_weight_sum_zero, Toast.LENGTH_LONG).show();
                return;
            }
			
			Random r_index = new Random();
			item_result_id = -1;

			//int lap_count, duration;
			//lap_count = Integer.valueOf(Main.app_settings.getString(Common.PrefKeys.POINTER_LAP_COUNT, Common.PointerProp.lap_count_default));
			//duration = Integer.valueOf(Main.app_settings.getString(Common.PrefKeys.POINTER_SPEED, Common.PointerProp.speed_medium_drt+""));
			angle_after = arrow.spin(r_index, random_degree_set, pointer_lap_count, pointer_duration);
			
			for(int i = 0 ; i < angle_list.size() ; i ++){
				if(angle_after > angle_list.get(i)){
					item_result_id++;
				}else{
					break;
				}
			}
			
			if( item_result_id < 0) item_result_id = 0;
			
		}
    	
    }

	@Override
	public void onColorSetFinish(ArrayList<Integer> color_set) {
		this.color_set = color_set;
		
		if( !color_update_flag ) return;
		update_color_list();
		if(drawer_color_list != null){	
			if(drawer_color_list.getAdapter() != null)	
				Common.DEBUG("list_adapter update");
				list_adapter.notifyDataSetChanged();
		}
	}
	
	private void update_color_list(){
		Iterator<HashMap<String, Object>> it_color_list = color_list.iterator();
		Iterator<Integer> it_color_set = color_set.iterator();
		
		while(it_color_list.hasNext()){
			HashMap<String, Object> tmp_item = it_color_list.next();
			
			if( tmp_item.get("color").equals(Color.BLACK) ){
				if(it_color_set.hasNext()){
					tmp_item.put("color", it_color_set.next());
				}
			}
		}
		
		color_update_flag = false;
	}
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.show_activity_color_list, menu);
    	
        return super.onCreateOptionsMenu(menu);
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item){
    	switch(item.getItemId()){
    	case R.id.color_list_drawer_toggle:
    		if( drawer.isDrawerOpen(drawer_layout) ){
    			drawer.closeDrawer(drawer_layout);
    		}else{
    			drawer.openDrawer(drawer_layout);
    		}
    		break;
    	}
    	
    	return super.onOptionsItemSelected(item);
    }
    
}
